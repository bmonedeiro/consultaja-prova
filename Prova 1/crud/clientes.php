<?php include 'header.php'; ?>

<script type="text/javascript">
$(document).ready(function(){
    $('#campos_juridico').hide(); 
	$('input[name=tipo]').change(function(){
		if ($(this).val()=="Fisico"){
          $('#campos_fisico').show();
          $('#campos_juridico').hide();         
        }else if ($(this).val()=="Juridico"){
          $('#campos_fisico').hide();
          $('#campos_juridico').show();
        }
	 });

  $('.detalhes').click(function(){
    $(this).nextUntil('tr.detalhes').Toggle();
  }); 

});
</script>
<?php
  $cliente = new Cliente();
  $grupo = new Grupo();
  
  if(isset($_POST['cadastrar'])):
    $cliente->setTipo($_POST['tipo']);
    $cliente->setIdGrupo($_POST['idgrupo']);
    $cliente->setNome($_POST['nome']);
    $cliente->setCpf($_POST['cpf']);
    if (isset($_POST['sexo'])){
      $cliente->setSexo($_POST['sexo']);
    }else{
       $cliente->setSexo(null);
    }
    $cliente->setDataNascimento($_POST['data_nascimento']);
    $cliente->setNomeFantasia($_POST['nome_fantasia']);
    $cliente->setRazaoSocial($_POST['razao_social']);
    $cliente->setCnpj($_POST['cnpj']);
    $cliente->setDataFundacao($_POST['data_fundacao']);

    # Insert
    if($cliente->insert()){
      header("Location: clientes.php");
    }
  endif;

  if(isset($_POST['atualizar'])):
    $cliente->setIdCliente($_POST['id']);
    $cliente->setTipo($_POST['tipo']);
    $cliente->setNome($_POST['idgrupo']);
    $cliente->setNome($_POST['nome']);
    $cliente->setCpf($_POST['cpf']);
     if (isset($_POST['sexo'])){
      $cliente->setSexo($_POST['sexo']);
    }else{
       $cliente->setSexo(null);
    }
    $cliente->setDataNascimento($_POST['data_nascimento']);
    $cliente->setNomeFantasia($_POST['nome_fantasia']);
    $cliente->setRazaoSocial($_POST['razao_social']);
    $cliente->setCnpj($_POST['cnpj']);
    $cliente->setDataFundacao($_POST['data_fundacao']);

    # Insert
    if($cliente->update()){
      header("Location: clientes.php");
    }
  endif; 

  if(isset($_GET['acao']) && $_GET['acao'] == 'deletar'):
    $id = (int)$_GET['id'];
    $cliente->setIdCliente($id);

    if($cliente->delete()){
      header("Location: clientes.php");
    }
  endif;

  if(isset($_GET['acao']) && $_GET['acao'] == 'editar'){ 
    $id = (int)$_GET['id'];
    $resultado = $cliente->find($id); ?>
    
    <div class="jumbotron">
    	<form method="post" action="" class="form-horizontal">
        	<input type="hidden" name="id" value="<?=$id?>">
        	<input type="hidden" name="tipo" value="<?=$resultado[0]->tipo?>">
          <input type="hidden" name="nome_fantasia" value="<?=$resultado[0]->nome_fantasia?>">
			    <input type="hidden" name="razao_social" value="<?=$resultado[0]->razao_social?>">
          <input type="hidden" name="cnpj" value="<?=$resultado[0]->cnpj?>">
          <input type="hidden" name="data_fundacao" value="<?=$resultado[0]->data_fundacao?>">
         	<input type="hidden" name="data_nascimento" value="<?=$resultado[0]->data_nascimento?>">
         	<input type="hidden" name="nome" value="<?=$resultado[0]->nome?>">
         	<input type="hidden" name="cpf" value="<?=$resultado[0]->cpf?>">
         	<input type="hidden" name="sexo" value="<?=$resultado[0]->sexo?>">
            <div class="cabecalho"><h3>Editar Cliente:</h3></div><br>
            <div class="form-group">
            <div class="input-group">
            <span class="input-group-addon" id="nome">Esse cliente pertence a algum grupo?</span>
              <select class="selectpicker form-control" name="idgrupo">
                <?php
                  foreach ($grupo->findAll() as $key => $value){ ?>
                    <option  value="<?=$value->idgrupo?>" <?=(isset($resultado[0]->idgrupo) && $resultado[0]->idgrupo==$value->idgrupo)?'selected':''?> ><?=$value->nome_grupo?></option>
                <?php } ?>
                <option <?=(!isset($resultado[0]->idgrupo))?'selected':''?> >Nenhum</option>
              </select>
            </div>
          </div>

            <?php if (!is_null($resultado[0]->nome)) { ?>
              <div class="form-group">
                <div class="input-group">
                  <span class="input-group-addon" for="nome">Nome</span>
                  <input type="text" class="form-control" name="nome" value="<?=$resultado[0]->nome?>" placeholder="Nome">
                </div>
                <br>
                <div class="input-group">
                  <span class="input-group-addon" for="cpf">CPF</span>
                  <input type="text" class="form-control" name="cpf" value="<?=$resultado[0]->cpf?>" placeholder="CPF">
                </div>
                <br>
                <div class="input-group">
                  <span class="input-group-addon" for="data_nascimento">Data Nascimento</span>
                  <input type="text" class="form-control" name="data_nascimento" value="<?=$resultado[0]->data_nascimento?>" placeholder="Data Nascimento">
                </div>
                <br>
                <div class="input-group">
                  <div class="radio col-lg-6">
                    <label><input type="radio" name="sexo" value="F" <?=($resultado[0]->sexo =="F")?'checked':''?> >Feminino</label>
                  </div>
                  <div class="radio col-lg-6">
                    <label><input type="radio" name="sexo" value="M" <?=($resultado[0]->sexo=="M")?'checked':''?> >Masculino</label>
                  </div>
                </div>
              </div>
            <?php } else { ?>
              	<div class="form-group">
                  <div class="input-group">
                    <span class="input-group-addon"  for="nome_fantasia">Nome Fantasia</span>
                    <input type="text" class="form-control" name="nome_fantasia" value="<?=$resultado[0]->nome_fantasia?>"placeholder="Nome Fantasia">
                  </div>
                  <br>
                  <div class="input-group">
                    <span class="input-group-addon" for="razao_social">Razão Social</span>
                    <input type="text" class="form-control" name="razao_social" value="<?=$resultado[0]->razao_social?>" placeholder="Razão Social">
                  </div>
                  <br>
                  <div class="input-group">
                    <span class="input-group-addon"  for="data_fundacao">Data Fundação</span>
                    <input type="text" class="form-control" name="data_fundacao" value="<?=$resultado[0]->data_fundacao?>"placeholder="Data Fundação">
                  </div>
                  <br>
                   <div class="input-group">
                    <span class="input-group-addon"  for="cnpj">CNPJ</span>
                    <input type="text" class="form-control" name="cnpj" value="<?=$resultado[0]->cnpj?>"placeholder="CNPJ">
                  </div>
              </div>
            <?php } ?>
            <input type="submit" name="atualizar" class="btn btn-primary" value="Atualizar dados">  
        </form>
    </div>
   	<?php
  } else if(isset($_GET['acao']) && $_GET['acao'] == 'cadastrar'){ ?>
    
    <div class="jumbotron">
      <form method="post" action="" class="form-horizontal">
          <div class="cabecalho"><h3>Cadastrar Cliente:</h3></div><br>
          <label>Qual é o tipo do cliente que você quer adicionar?</label>
          <div class="form-group" value="Fisico">
            <div class="radio col-lg-6">
              <label><input type="radio" name="tipo" id="fisico" value="Fisico" checked>Físico</label>
            </div>
            <div class="radio col-lg-6">
              <label><input type="radio" name="tipo" id="juridico" value="Juridico">Jurídico</label>
            </div>
          </div>
          <div class="form-group">
            <div class="input-group">
            <span class="input-group-addon" id="nome">Esse cliente pertence a algum grupo?</span>
              <select class="selectpicker form-control" name="idgrupo">
                <?php
                  foreach ($grupo->findAll() as $key => $value){ ?>
                    <option value="<?=$value->idgrupo?>"><?=$value->nome_grupo?></option>
                  <?php 
                  }
                ?>
                <option>Nenhum</option>
              </select>
            </div>
          </div>
          <div class="form-group" id="campos_fisico">
            <div class="input-group">
              <span class="input-group-addon" id="nome">Nome</span>
              <input type="text" class="form-control" name="nome" placeholder="Nome">
            </div>
            <br>
            <div class="input-group">
              <span class="input-group-addon" for="cpf" >CPF</span>
              <input type="text" class="form-control" name="cpf" placeholder="CPF">
            </div>
            <br>
            <div class="input-group">
              <span class="input-group-addon" for="data_nascimento">Data Nascimento</span>
              <input type="text" class="form-control" name="data_nascimento" placeholder="Data Nascimento">
            </div>
            <br>
            <div class="input-group" >
              <div class="radio col-lg-6">
                <label><input type="radio" name="sexo" value="F">Feminino</label>
              </div>
              <div class="radio col-lg-6">
                <label><input type="radio" name="sexo" value="M">Masculino</label>
              </div>
            </div>
          </div>
          <div class="form-group" id="campos_juridico">
              <div class="input-group">
                <span class="input-group-addon" for="nome_fantasia">Nome Fantasia</span>
                <input type="text" class="form-control" name="nome_fantasia" placeholder="Nome Fantasia">
              </div>
              <br>
              <div class="input-group">
                <span class="input-group-addon" for="razao_social">Razão Social</span>
                <input type="text" class="form-control" name="razao_social" placeholder="Razão Social">
              </div>
              <br>
              <div class="input-group">
                <span class="input-group-addon" for="data_fundacao">Data Fundação</span>
                <input type="text" class="form-control" name="data_fundacao" placeholder="Data Fundação">
              </div>
              <br>
               <div class="input-group">
                <span class="input-group-addon"for="cnpj">CNPJ</span>
                <input type="text" class="form-control" name="cnpj" placeholder="CNPJ">
              </div>
          </div>
          <input type="submit" name="cadastrar" class="btn btn-primary" value="Cadastrar cliente">
      </form>
    </div>
  <?php } ?>  
  <div class="conteiner">  
	  <div class="well ">
	    <a class="btn btn-success pull-right" style="float" href='clientes.php?acao=cadastrar'><i class=" glyphicon glyphicon-plus"></i> Adicionar cliente</a>
	    <div class="cabecalho"><h3>Clientes:</h3></div><br>
	    <?php
      	$rowCount = count($cliente->findAll());
      	if($rowCount>=1){ ?>
		    <table class="table table-hover">
		    <?php
		    $cliente = new Cliente();
		      foreach ($cliente->findAll() as $key => $value): 
		        $idcl = $value->idcliente;
		        ?>
		        <tbody>
		          <tr>
		            <?php if($value->nome!==null) { ?>
		              <td><label>Nome: </label><?=$value->nome?></td>
		            <?php }else{ ?>
		              <td ><label>Nome Fantasia: </label><?=$value->nome_fantasia?></td>
		            <?php } ?>
		            <td><label>Tipo: </label><?=$value->tipo?></td>
		            
		            <td colspan="3">
		              <button style="align: right" class="btn btn-default" type="button" data-toggle="collapse" data-target="#<?=$idcl?>" aria-expanded="false" aria-controls="<?=$idcl?>"><i class="glyphicon glyphicon-plus"></i></button>
		              <a class="btn btn-default" href='clientes.php?acao=editar&id=<?=$value->idcliente?>'><i class=" glyphicon glyphicon-edit"></i></a>
		              <a class="btn btn-default" href='clientes.php?acao=deletar&id=<?=$value->idcliente?>'> <i class="glyphicon glyphicon-remove"></i></a>

		            </td>
		          </tr>   
		          <tr  class="detalhes collapse" id="<?=$idcl?>">
		            <?php if ($value->nome_grupo!==null){ ?>
		            <td><label>Grupo: </label><?=$value->nome_grupo?></td>
		            <?php }else{ ?>
		              <td><label>Grupo: </label>Nenhum grupo</td>
		            <?php }
		            if($value->cpf!==null){//pessoa física ?>
		              <td ><label>CPF: </label><?=$value->cpf?></td>
		              <td ><label>Sexo: </label><?=$value->sexo?></td>
		              <td colspan="3"><label>Data de Nascimento :</label><?=$value->data_nascimento?></td>

		            <?php }else{//pessoa juridica ?>
		              <td ><label>Razão Social: </label><?=$value->razao_social?></td>
		              <td ><label>cnpj: </label><?=$value->cnpj?></td>
		              <td colspan="3"><label>Data de Fundação :</label><?=$value->data_fundacao?></td>
		            <?php }
		            ?>              
		          </tr>
		        </tbody>
		      <?php endforeach; 
		    }else{  ?>
		    <p> Não há nenhum cliente cadastrado ainda! :( 
		    <?php } ?>
	    </div>
	  </table>
    </div>
  </div>


<?php include 'footer.php'; ?>