<?php include 'header.php'; ?>
<script type="text/javascript">
  $(document).ready(function(){  
    $('.detalhes').click(function(){
      $(this).nextUntil('tr.detalhes').Toggle();
    }); 
  });
</script>
<?php
  $grupo = new Grupo();
  
  if(isset($_POST['cadastrar'])):
    $grupo->setNomeGrupo($_POST['nome_grupo']);
    if($grupo->insert()){
      header("Location: grupos.php");
    }
  endif;

  if(isset($_POST['atualizar'])):
    $grupo->setNomeGrupo($_POST['nome_grupo']);
    $grupo->setIdGrupo($_POST['id']);
    # Insert
    if($grupo->update()){
      header("Location: grupos.php");
    }
  endif; 

  if(isset($_GET['acao']) && $_GET['acao'] == 'deletar'):
    $id = (int)$_GET['id'];
    $grupo->setIdgrupo($id);

    if($grupo->delete()){
      header("Location: grupos.php");
    }
  endif;

  if(isset($_GET['acao']) && $_GET['acao'] == 'editar'){ 
    $id = (int)$_GET['id'];
    $resultado = $grupo->find($id); ?>
    
    <div class="jumbotron">
    	<form method="post" action="" class="form-horizontal">
        	<input type="hidden" name="id" value="<?=$id?>">
         	<input type="hidden" name="nome_grupo" value="<?=$resultado[0]->nome_grupo?>">
           <div class="cabecalho"><h3>Editar Grupo:</h3></div><br>
            <div class="input-group">
              <span class="input-group-addon" for="nome_grupo" class="control-label">Nome Grupo</span>
                <input type="text" class="form-control" name="nome_grupo" value="<?=$resultado[0]->nome_grupo?>" placeholder="Nome Grupo">
            </div>
            <br>
            <input type="submit" name="atualizar" class="btn btn-primary" value="Atualizar dados">  
        </form>
    </div>
   	<?php
  } else if(isset($_GET['acao']) && $_GET['acao'] == 'cadastrar'){ ?>
    
    <div class="jumbotron">
      <form method="post" action="" class="form-horizontal">
        <div class="cabecalho"><h3>Cadastrar Grupo:</h3></div><br>
        <div class="input-group">
          <span class="input-group-addon" for="nome_grupo" class="control-label">Nome Grupo</span>
          <input type="text" class="form-control" name="nome_grupo" placeholder="Nome Grupo">
        </div>
        <br>
        <input type="submit" name="cadastrar" class="btn btn-primary" value="Cadastrar grupo">
      </form>
    </div>
  <?php } ?>  

  <div class="conteiner">  
	  <div class="well ">
	    <a class="btn btn-success pull-right" style="float" href='grupos.php?acao=cadastrar'><i class=" glyphicon glyphicon-plus"></i> Adicionar Grupo</a>
	    <h3> Grupos:</h3>
	    <?php
      	$rowCount = count($grupo->findAll());
      	if($rowCount>=1){ ?>
		    <table class="table table-hover">
		    <?php
		    $grupo = new Grupo();
		      foreach ($grupo->findAll() as $key => $value): 
		        $idg = $value->idgrupo;
		        ?>
		        <tbody>
		          <tr>
                <td><label>Id: </label><?=$value->idgrupo?></td>
		            <td><label>Nome: </label><?=$value->nome_grupo?></td>
		            <td >
                  <a class="btn btn-default pull-right" href='grupos.php?acao=deletar&id=<?=$value->idgrupo?>'> <i class="glyphicon glyphicon-remove"></i></a>
                  <a class="btn btn-default pull-right" href='grupos.php?acao=editar&id=<?=$value->idgrupo?>'><i class=" glyphicon glyphicon-edit"></i></a>
                  <button  class="btn btn-default pull-right" type="button" data-toggle="collapse" data-target="#<?=$idg?>" aria-expanded="false" aria-controls="<?=$idg?>"><i class="glyphicon glyphicon-plus"></i></button>
                  
		            </td>
		          </tr> 
              <tr class="detalhes collapse" id="<?=$idg?>">
                <td colspan="3">
                  <?php 
                  $rowCount = count($grupo->findByGroup($idg));
                  if($rowCount>=1){
                    foreach ($grupo->findByGroup($idg) as $key => $val): 
                      if ($val->nome!==null){ ?>
                      <p>
                      <label>Id Cliente:</label><?=$val->idcliente?><br>
                      <label>Tipo:</label><?=$val->tipo?><br>
                      <label>Nome:</label><?=$val->nome?><br>
                      <label>CPF:</label><?=$val->cpf?><br>
                      <label>Data Nascimento:</label><?=$val->data_nascimento?><br>
                      <label>Sexo:</label><?=$val->sexo?><br>
                      <div class="clearfix"></div>
                    </p>
                    <?php
                    }else{
                      ?>
                      <p>
                        <label>Id Cliente:</label><?=$val->idcliente?><br>
                        <label>Tipo:</label><?=$val->tipo?><br>
                        <label>Nome Fantasia:</label><?=$val->nome_fantasia?><br>
                        <label>Razão Social:</label><?=$val->razao_social?><br>
                        <label>CNPJ:</label><?=$val->cnpj?><br>
                        <label>Data Fundação:</label><?=$val->data_fundacao?>
                        <div class="clearfix"></div>
                      </p>
                    <?php 
                    }
                    endforeach; 
                  }else{ ?>
                    <p>Não possui nenhum cliente cadastrado</p>
                    <?php 
                  } ?>
                </td>
              </tr>  
		        </tbody>
		        <?php 
          endforeach; 
		    }else{  ?>
		    <p> Não há nenhum grupo cadastrado ainda! :( 
		    <?php } ?>
	    </div>
	  </table>
    </div>
  </div>


<?php include 'footer.php'; ?>