CREATE DATABASE  IF NOT EXISTS `mydb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `mydb`;
-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: mydb
-- ------------------------------------------------------
-- Server version	5.7.9

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cliente` (
  `idcliente` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(45) NOT NULL,
  `idgrupo` int(11) DEFAULT NULL,
  PRIMARY KEY (`idcliente`),
  KEY `fk_cliente_grupo1_idx` (`idgrupo`),
  CONSTRAINT `fk_cliente_grupo1` FOREIGN KEY (`idgrupo`) REFERENCES `grupo` (`idgrupo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cliente_fisico`
--

DROP TABLE IF EXISTS `cliente_fisico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cliente_fisico` (
  `nome` varchar(150) NOT NULL,
  `cpf` varchar(15) NOT NULL,
  `sexo` char(1) DEFAULT NULL,
  `data_nascimento` varchar(12) DEFAULT NULL,
  `idcliente` int(11) NOT NULL,
  PRIMARY KEY (`idcliente`),
  KEY `fk_cliente_fisico_cliente_idx` (`idcliente`),
  CONSTRAINT `fk_cliente_fisico_cliente` FOREIGN KEY (`idcliente`) REFERENCES `cliente` (`idcliente`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cliente_juridico`
--

DROP TABLE IF EXISTS `cliente_juridico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cliente_juridico` (
  `nome_fantasia` varchar(100) NOT NULL,
  `razao_social` varchar(100) DEFAULT NULL,
  `cnpj` varchar(25) NOT NULL,
  `data_fundacao` varchar(12) DEFAULT NULL,
  `idcliente` int(11) NOT NULL,
  PRIMARY KEY (`idcliente`),
  CONSTRAINT `fk_cliente_juridico_cliente1` FOREIGN KEY (`idcliente`) REFERENCES `cliente` (`idcliente`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `grupo`
--

DROP TABLE IF EXISTS `grupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grupo` (
  `idgrupo` int(11) NOT NULL AUTO_INCREMENT,
  `nome_grupo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idgrupo`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'mydb'
--
/*!50003 DROP PROCEDURE IF EXISTS `alterarCliente` */;
ALTER DATABASE `mydb` CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `alterarCliente`(
	-- Lista de parametros
    IN id int(11),
    IN tipo varchar(45),    
    IN idgrupo int(11),
	IN nome varchar(150),
    IN cpf varchar(15),
    IN sexo char(1),
    IN data_nascimento varchar(12),
    IN nome_fantasia varchar(100),
    IN razao_social varchar(100),
    IN cnpj varchar(25),
    IN data_fundacao varchar(12)
)
procedure_a:
BEGIN
-- Define variável para efetuar Rollback caso ocorra algum erro de execução
    DECLARE `_rollback` BOOL DEFAULT 0;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
    
    -- Faz verificações
	IF ((tipo IS NULL) OR (tipo = ""))
	THEN 
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Informar o Tipo do Contato';
        LEAVE procedure_a;
	END IF; 
    
    -- Insere no banco
    START TRANSACTION;
		-- Insere o cliente no tabela cliente
		UPDATE `cliente` SET `tipo` = tipo, `idgrupo` = idgrupo WHERE `idcliente` = id;

		-- Se o tipo do contato for "pessoa fisica", insere na tabela de pessoas fisicas
        IF (tipo like "fisico") THEN
			UPDATE`cliente_fisico` SET `nome` = nome,`cpf` = cpf,`sexo` = sexo,`data_nascimento` = data_nascimento WHERE `idcliente` = id;        
		-- Se o tipo do contato for "pessoa juridica", insere na tabela de pessoas juridicas            
        ELSEIF (tipo like "juridico") THEN
			UPDATE`cliente_juridico` SET `nome_fantasia` = nome_fantasia,`razao_social` = razao_social,`cnpj` = cnpj,`data_fundacao` = data_fundacao WHERE `idcliente` = id;                
        -- Se o tipo do contato for outro, cancela a transação, pois trata-se de um tipo inválido
		ELSE 
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Tipo Inválido';
			ROLLBACK;
		END IF;

	-- Se ocorreu algum erro durante a transação, efetua o rollback
	IF `_rollback` THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Rollback';    
		ROLLBACK;       
	ELSE
	-- Se NÃO ocorreu nenhum erro durante a transação, grava os dados
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Commit';      
		COMMIT;
	END IF;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE `mydb` CHARACTER SET utf8 COLLATE utf8_general_ci ;
/*!50003 DROP PROCEDURE IF EXISTS `alterarGrupo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `alterarGrupo`(IN nome varchar(45), IN id INT(11))
BEGIN
	UPDATE `grupo` SET `nome_grupo`=nome WHERE `idgrupo`=id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `buscaCliente` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscaCliente`(IN id int(11))
BEGIN
	select  C.idcliente, 
			C.tipo, 
            C.idgrupo, 
            CJ.nome_fantasia,
            CJ.razao_social,
            CJ.cnpj,
            CJ.data_fundacao,
            CF.nome,
            CF.cpf,
            CF.sexo,
            CF.data_nascimento           
    from cliente AS C 
    LEFT JOIN cliente_juridico AS CJ ON (C.idcliente = CJ.idcliente) 
    LEFT JOIN cliente_fisico AS CF ON (C.idcliente = CF.idcliente)
    WHERE C.idcliente = id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `buscaClientesGrupo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscaClientesGrupo`(IN id int (11))
BEGIN
	select  C.idcliente, 
			C.tipo, 
            C.idgrupo, 
            CJ.nome_fantasia,
            CJ.razao_social,
            CJ.cnpj,
            CJ.data_fundacao,
            CF.nome,
            CF.cpf,
            CF.sexo,
            CF.data_nascimento           
    from cliente AS C 
    LEFT JOIN cliente_juridico AS CJ ON (C.idcliente = CJ.idcliente) 
    LEFT JOIN cliente_fisico AS CF ON (C.idcliente = CF.idcliente)
    WHERE C.idgrupo = id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `buscaGrupo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscaGrupo`(IN id int(11))
BEGIN
	SELECT * FROM grupo WHERE idgrupo=id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `buscaTodosClientes` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscaTodosClientes`()
BEGIN
	select  C.idcliente, 
			C.tipo, 
            C.idgrupo, 
            CJ.nome_fantasia,
            CJ.razao_social,
            CJ.cnpj,
            CJ.data_fundacao,
            CF.nome,
            CF.cpf,
            CF.sexo,
            CF.data_nascimento,   
            G.nome_grupo
    from cliente AS C 
    LEFT JOIN cliente_juridico AS CJ ON (C.idcliente = CJ.idcliente) 
    LEFT JOIN cliente_fisico AS CF ON (C.idcliente = CF.idcliente)
    LEFT JOIN grupo AS G ON (C.idgrupo = G.idgrupo);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `buscaTodosGrupos` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `buscaTodosGrupos`()
BEGIN
	SELECT * FROM grupo;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `deletarCliente` */;
ALTER DATABASE `mydb` CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `deletarCliente`(IN id int(11))
BEGIN
-- Define variável para efetuar Rollback caso ocorra algum erro de execução
    DECLARE `_rollback` BOOL DEFAULT 0;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
    
    START TRANSACTION;
		DELETE FROM `cliente_juridico` WHERE `idcliente` = id;
        DELETE FROM `cliente_fisico` WHERE `idcliente` = id;
        DELETE FROM `cliente` WHERE `idcliente` = id;
	-- Se ocorreu algum erro durante a transação, efetua o rollback
	IF `_rollback` THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Rollback';    
		ROLLBACK;       
	ELSE
	-- Se NÃO ocorreu nenhum erro durante a transação, grava os dados
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Commit';      
		COMMIT;
	END IF;
        
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE `mydb` CHARACTER SET utf8 COLLATE utf8_general_ci ;
/*!50003 DROP PROCEDURE IF EXISTS `deletarGrupo` */;
ALTER DATABASE `mydb` CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `deletarGrupo`(IN id INT(11))
BEGIN
-- Define variável para efetuar Rollback caso ocorra algum erro de execução
    DECLARE `_rollback` BOOL DEFAULT 0;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
    
    START TRANSACTION;
		UPDATE `cliente` SET`idgrupo`= null WHERE `idgrupo`=id;
		DELETE FROM `grupo` WHERE `idgrupo` = id;
        -- Se ocorreu algum erro durante a transação, efetua o rollback
	IF `_rollback` THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Rollback';    
		ROLLBACK;       
	ELSE
	-- Se NÃO ocorreu nenhum erro durante a transação, grava os dados
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Commit';      
		COMMIT;
	END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
ALTER DATABASE `mydb` CHARACTER SET utf8 COLLATE utf8_general_ci ;
/*!50003 DROP PROCEDURE IF EXISTS `inserirCliente` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inserirCliente`(
	-- Lista de parametros
    IN tipo varchar(45),    
    IN idgrupo int(11),
	IN nome varchar(150),
    IN cpf varchar(15),
    IN sexo char(1),
    IN data_nascimento varchar(12),
    IN nome_fantasia varchar(100),
    IN razao_social varchar(100),
    IN cnpj varchar(25),
    IN data_fundacao varchar(12)
)
procedure_a:
BEGIN

	-- Define variável para efetuar Rollback caso ocorra algum erro de execução
    DECLARE idcliente int(11);
    DECLARE `_rollback` BOOL DEFAULT 0;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
    
    -- Faz verificações
	IF ((tipo IS NULL) OR (tipo = ""))
	THEN 
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Informar o Tipo do Cliente';
        LEAVE procedure_a;
	END IF; 
    
    -- Insere no banco
    START TRANSACTION;
		-- Insere o cliente no tabela cliente
		INSERT INTO `cliente` (`tipo`,`idgrupo`) VALUES (tipo, idgrupo);

		-- Obtem o id do cliente inserido
        SET idcliente = LAST_INSERT_ID();

		-- Se o tipo do contato for "pessoa fisica", insere na tabela de pessoas fisicas
        IF (tipo like "Fisico") THEN
			INSERT INTO `cliente_fisico` (`idcliente`,`nome`,`cpf`,`sexo`,`data_nascimento`) VALUES (idcliente,nome,cpf,sexo,data_nascimento);
		-- Se o tipo do contato for "pessoa juridica", insere na tabela de pessoas juridicas            
        ELSEIF (tipo like "Juridico") THEN
			INSERT INTO `cliente_juridico` (`idcliente`,`nome_fantasia`,`razao_social`,`cnpj`,`data_fundacao`) VALUES (idcliente,nome_fantasia,razao_social,cnpj,data_fundacao);
        -- Se o tipo do contato for outro, cancela a transação, pois trata-se de um tipo inválido
		ELSE 
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Tipo Inválido';
			ROLLBACK;
		END IF;

	-- Se ocorreu algum erro durante a transação, efetua o rollback
	IF `_rollback` THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Rollback';    
		ROLLBACK;       
	ELSE
	-- Se NÃO ocorreu nenhum erro durante a transação, grava os dados
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Commit';      
		COMMIT;
	END IF;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `inserirGrupo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `inserirGrupo`(IN nome_grupo varchar(45))
BEGIN
 INSERT INTO `grupo` (`nome_grupo`) VALUES (nome_grupo);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-09-29 14:27:45
