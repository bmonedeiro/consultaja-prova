CREATE DEFINER=`root`@`localhost` PROCEDURE `buscaTodosClientes`()
BEGIN
	select  C.idcliente, 
			C.tipo, 
            C.idgrupo, 
            CJ.nome_fantasia,
            CJ.razao_social,
            CJ.cnpj,
            CJ.data_fundacao,
            CF.nome,
            CF.cpf,
            CF.sexo,
            CF.data_nascimento,   
            G.nome_grupo
    from cliente AS C 
    LEFT JOIN cliente_juridico AS CJ ON (C.idcliente = CJ.idcliente) 
    LEFT JOIN cliente_fisico AS CF ON (C.idcliente = CF.idcliente)
    LEFT JOIN grupo AS G ON (C.idgrupo = G.idgrupo);
END