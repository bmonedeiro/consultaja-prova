CREATE DEFINER=`root`@`localhost` PROCEDURE `deletarCliente`(IN id int(11))
BEGIN
-- Define variável para efetuar Rollback caso ocorra algum erro de execução
    DECLARE `_rollback` BOOL DEFAULT 0;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
    
    START TRANSACTION;
		DELETE FROM `cliente_juridico` WHERE `idcliente` = id;
        DELETE FROM `cliente_fisico` WHERE `idcliente` = id;
        DELETE FROM `cliente` WHERE `idcliente` = id;
	-- Se ocorreu algum erro durante a transação, efetua o rollback
	IF `_rollback` THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Rollback';    
		ROLLBACK;       
	ELSE
	-- Se NÃO ocorreu nenhum erro durante a transação, grava os dados
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Commit';      
		COMMIT;
	END IF;
        
END