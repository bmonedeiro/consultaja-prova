CREATE DEFINER=`root`@`localhost` PROCEDURE `buscaClientesGrupo`(IN id int (11))
BEGIN
	select  C.idcliente, 
			C.tipo, 
            C.idgrupo, 
            CJ.nome_fantasia,
            CJ.razao_social,
            CJ.cnpj,
            CJ.data_fundacao,
            CF.nome,
            CF.cpf,
            CF.sexo,
            CF.data_nascimento           
    from cliente AS C 
    LEFT JOIN cliente_juridico AS CJ ON (C.idcliente = CJ.idcliente) 
    LEFT JOIN cliente_fisico AS CF ON (C.idcliente = CF.idcliente)
    WHERE C.idgrupo = id;
END