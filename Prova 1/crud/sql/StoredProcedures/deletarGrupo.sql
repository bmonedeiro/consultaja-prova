CREATE DEFINER=`root`@`localhost` PROCEDURE `deletarGrupo`(IN id INT(11))
BEGIN
-- Define variável para efetuar Rollback caso ocorra algum erro de execução
    DECLARE `_rollback` BOOL DEFAULT 0;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
    
    START TRANSACTION;
		UPDATE `cliente` SET`idgrupo`= null WHERE `idgrupo`=id;
		DELETE FROM `grupo` WHERE `idgrupo` = id;
        -- Se ocorreu algum erro durante a transação, efetua o rollback
	IF `_rollback` THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Rollback';    
		ROLLBACK;       
	ELSE
	-- Se NÃO ocorreu nenhum erro durante a transação, grava os dados
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Commit';      
		COMMIT;
	END IF;
END