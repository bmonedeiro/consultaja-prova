CREATE DEFINER=`root`@`localhost` PROCEDURE `inserirCliente`(
	-- Lista de parametros
    IN tipo varchar(45),    
    IN idgrupo int(11),
	IN nome varchar(150),
    IN cpf varchar(15),
    IN sexo char(1),
    IN data_nascimento varchar(12),
    IN nome_fantasia varchar(100),
    IN razao_social varchar(100),
    IN cnpj varchar(25),
    IN data_fundacao varchar(12)
)
procedure_a:
BEGIN

	-- Define variável para efetuar Rollback caso ocorra algum erro de execução
    DECLARE idcliente int(11);
    DECLARE `_rollback` BOOL DEFAULT 0;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
    
    -- Faz verificações
	IF ((tipo IS NULL) OR (tipo = ""))
	THEN 
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Informar o Tipo do Cliente';
        LEAVE procedure_a;
	END IF; 
    
    -- Insere no banco
    START TRANSACTION;
		-- Insere o cliente no tabela cliente
		INSERT INTO `cliente` (`tipo`,`idgrupo`) VALUES (tipo, idgrupo);

		-- Obtem o id do cliente inserido
        SET idcliente = LAST_INSERT_ID();

		-- Se o tipo do contato for "pessoa fisica", insere na tabela de pessoas fisicas
        IF (tipo like "Fisico") THEN
			INSERT INTO `cliente_fisico` (`idcliente`,`nome`,`cpf`,`sexo`,`data_nascimento`) VALUES (idcliente,nome,cpf,sexo,data_nascimento);
		-- Se o tipo do contato for "pessoa juridica", insere na tabela de pessoas juridicas            
        ELSEIF (tipo like "Juridico") THEN
			INSERT INTO `cliente_juridico` (`idcliente`,`nome_fantasia`,`razao_social`,`cnpj`,`data_fundacao`) VALUES (idcliente,nome_fantasia,razao_social,cnpj,data_fundacao);
        -- Se o tipo do contato for outro, cancela a transação, pois trata-se de um tipo inválido
		ELSE 
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Tipo Inválido';
			ROLLBACK;
		END IF;

	-- Se ocorreu algum erro durante a transação, efetua o rollback
	IF `_rollback` THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Rollback';    
		ROLLBACK;       
	ELSE
	-- Se NÃO ocorreu nenhum erro durante a transação, grava os dados
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Commit';      
		COMMIT;
	END IF;


END