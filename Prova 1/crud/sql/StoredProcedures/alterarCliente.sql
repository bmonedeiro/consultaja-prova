CREATE DEFINER=`root`@`localhost` PROCEDURE `alterarCliente`(
	-- Lista de parametros
    IN id int(11),
    IN tipo varchar(45),    
    IN idgrupo int(11),
	IN nome varchar(150),
    IN cpf varchar(15),
    IN sexo char(1),
    IN data_nascimento varchar(12),
    IN nome_fantasia varchar(100),
    IN razao_social varchar(100),
    IN cnpj varchar(25),
    IN data_fundacao varchar(12)
)
procedure_a:
BEGIN
-- Define variável para efetuar Rollback caso ocorra algum erro de execução
    DECLARE `_rollback` BOOL DEFAULT 0;
    DECLARE CONTINUE HANDLER FOR SQLEXCEPTION SET `_rollback` = 1;
    
    -- Faz verificações
	IF ((tipo IS NULL) OR (tipo = ""))
	THEN 
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Informar o Tipo do Contato';
        LEAVE procedure_a;
	END IF; 
    
    -- Insere no banco
    START TRANSACTION;
		-- Insere o cliente no tabela cliente
		UPDATE `cliente` SET `tipo` = tipo, `idgrupo` = idgrupo WHERE `idcliente` = id;

		-- Se o tipo do contato for "pessoa fisica", insere na tabela de pessoas fisicas
        IF (tipo like "fisico") THEN
			UPDATE`cliente_fisico` SET `nome` = nome,`cpf` = cpf,`sexo` = sexo,`data_nascimento` = data_nascimento WHERE `idcliente` = id;        
		-- Se o tipo do contato for "pessoa juridica", insere na tabela de pessoas juridicas            
        ELSEIF (tipo like "juridico") THEN
			UPDATE`cliente_juridico` SET `nome_fantasia` = nome_fantasia,`razao_social` = razao_social,`cnpj` = cnpj,`data_fundacao` = data_fundacao WHERE `idcliente` = id;                
        -- Se o tipo do contato for outro, cancela a transação, pois trata-se de um tipo inválido
		ELSE 
			SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Tipo Inválido';
			ROLLBACK;
		END IF;

	-- Se ocorreu algum erro durante a transação, efetua o rollback
	IF `_rollback` THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Rollback';    
		ROLLBACK;       
	ELSE
	-- Se NÃO ocorreu nenhum erro durante a transação, grava os dados
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'Commit';      
		COMMIT;
	END IF;


END