<?php
require_once 'Config.php';

class Cliente {
	//atributos globais de clientes
	private $tipo;
	private $idgrupo;
	private $idcliente;
	//atributos de um cliente fisico
	private $nome;
	private $cpf;
	private $sexo;
	private $data_nascimento;
	//atributos de um cliente juridico
	private $nome_fantasia;
	private $razao_social;
	private $cnpj;
	private $data_fundacao;

	public function setIdCliente($idcliente){
		$this->idcliente = $idcliente;
	}
	public function setTipo($tipo){
		$this->tipo = $tipo;
	}

	public function setIdGrupo($idgrupo){
		$this->idgrupo = $idgrupo;
	}

	public function setNome($nome){
		$this->nome = $nome;
	}

	public function setCpf($cpf){
		$this->cpf = $cpf;
	}

	public function setSexo($sexo){
		$this->sexo = $sexo;
	}

	public function setDataNascimento($data_nascimento){
		$this->data_nascimento = $data_nascimento;
	}

	public function setNomeFantasia($nome_fantasia){
		$this->nome_fantasia = $nome_fantasia;
	}

	public function setRazaoSocial($razao_social){
		$this->razao_social = $razao_social;
	}

	public function setCnpj($cnpj){
		$this->cnpj = $cnpj;
	}

	public function setDataFundacao($data_fundacao){
		$this->data_fundacao = $data_fundacao;
	}

	public function find($id){
		$conn = getConexao();
		$stmt = $conn->prepare("call mydb.buscaCliente(?)");
		$stmt->bindParam(1, $id, PDO::PARAM_STR, 4000); 
		$stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_OBJ);
		return ($result);
	}

	public function findAll(){

		$conn = getConexao();
		$stmt = $conn->prepare("call mydb.buscaTodosClientes()");
		$stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_OBJ);
		return ($result);
	}

	public function insert(){
		$conn = getConexao();
		$stmt = $conn->prepare("call mydb.inserirCliente(?,?,?,?,?,?,?,?,?,?)");
		$stmt->bindParam(1, $this->tipo, PDO::PARAM_STR); 
		$stmt->bindParam(2, $this->idgrupo, PDO::PARAM_INT); 
		$stmt->bindParam(3, $this->nome, PDO::PARAM_STR); 
		$stmt->bindParam(4, $this->cpf, PDO::PARAM_STR); 
		$stmt->bindParam(5, $this->sexo, PDO::PARAM_STR); 
		$stmt->bindParam(6, $this->data_nascimento, PDO::PARAM_STR); 
		$stmt->bindParam(7, $this->nome_fantasia, PDO::PARAM_STR); 
		$stmt->bindParam(8, $this->razao_social, PDO::PARAM_STR); 
		$stmt->bindParam(9, $this->cnpj, PDO::PARAM_STR); 
		$stmt->bindParam(10, $this->data_fundacao, PDO::PARAM_STR); 
		return ($stmt->execute());
	}
	
	public function update(){
		$conn = getConexao();
		$stmt = $conn->prepare("call mydb.alterarCliente(?,?,?,?,?,?,?,?,?,?,?)");
		$stmt->bindParam(1, $this->idcliente, PDO::PARAM_INT); 
		$stmt->bindParam(2, $this->tipo, PDO::PARAM_STR); 
		$stmt->bindParam(3, $this->idgrupo, PDO::PARAM_INT); 
		$stmt->bindParam(4, $this->nome, PDO::PARAM_STR); 
		$stmt->bindParam(5, $this->cpf, PDO::PARAM_STR); 
		$stmt->bindParam(6, $this->sexo, PDO::PARAM_STR); 
		$stmt->bindParam(7, $this->data_nascimento, PDO::PARAM_STR); 
		$stmt->bindParam(8, $this->nome_fantasia, PDO::PARAM_STR); 
		$stmt->bindParam(9, $this->razao_social, PDO::PARAM_STR); 
		$stmt->bindParam(10, $this->cnpj, PDO::PARAM_STR); 
		$stmt->bindParam(11, $this->data_fundacao, PDO::PARAM_STR); 
		return ($stmt->execute());
	}

	public function delete(){
		$conn = getConexao();
		$stmt = $conn->prepare("call mydb.deletarCliente(?)");
		$stmt->bindParam(1, $this->idcliente, PDO::PARAM_INT);
		return ($stmt->execute()); 
	}
}
?>