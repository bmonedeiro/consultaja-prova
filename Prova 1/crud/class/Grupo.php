<?php
require_once 'Config.php';

class Grupo {
	protected $table_cliente = 'grupo';
	private $idgrupo;
	private $nome_grupo;

	public function setIdGrupo($idgrupo){
		$this->idgrupo = $idgrupo;
	}

	public function setNomeGrupo($nome_grupo){
		$this->nome_grupo = $nome_grupo;
	}

	public function find($id){
		$conn = getConexao();
		$stmt = $conn->prepare("call mydb.buscaGrupo(?)");
		$stmt->bindParam(1, $id, PDO::PARAM_STR); 
		$stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_OBJ);
		return ($result);
	}

	public function findAll(){
		$conn = getConexao();
		$stmt = $conn->prepare("call mydb.buscaTodosGrupos()");
		$stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_OBJ);
		return ($result);
	}
	public function findByGroup($id){
		$conn = getConexao();
		$stmt = $conn->prepare("call mydb.buscaClientesGrupo(?)");
		$stmt->bindParam(1, $id, PDO::PARAM_INT); 
		$stmt->execute();
		$result = $stmt->fetchAll(PDO::FETCH_OBJ);
		return ($result);
	}

	public function delete(){
		$conn = getConexao();
		$stmt = $conn->prepare("call mydb.deletarGrupo(?)");
		$stmt->bindParam(1, $this->idgrupo, PDO::PARAM_INT);
		return ($stmt->execute()); 
	}

	public function insert(){
		$conn = getConexao();
		$stmt = $conn->prepare("call mydb.inserirGrupo(?)");
		$stmt->bindParam(1, $this->nome_grupo, PDO::PARAM_STR); 
		return ($stmt->execute());
	}
	public function update(){
		$conn = getConexao();
		$stmt = $conn->prepare("call mydb.alterarGrupo(?,?)");
		$stmt->bindParam(1, $this->nome_grupo, PDO::PARAM_STR); 
		$stmt->bindParam(2, $this->idgrupo, PDO::PARAM_INT); 
		return ($stmt->execute());
	}
}
?>