<?php

define ('DB_HOST', 'localhost');
define ('DB_NAME','mydb');
define ('DB_USER','root');
define ('DB_PASS','');

function getConexao(){
	try { 
	  $opcoes = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8');
	  $con = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME, DB_USER, DB_PASS,$opcoes); 
	  $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	  return $con;
	}
	catch (Exception $e) {
	  echo($e->getMessage());
	}
}