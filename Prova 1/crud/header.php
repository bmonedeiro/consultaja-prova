<?php 
  header('Content-Type: text/html; charset=utf-8');
  date_default_timezone_set('America/Sao_Paulo');
  setlocale (LC_ALL, 'pt_BR');

  function __autoload($class_name){
    require_once 'class/'.$class_name.'.php';
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!--Include bootstrap-->
      <meta http-equiv="content-type" content="text/html; charset=utf-8">
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <meta name="description" content="Teste Prático Consulta Já">
      <meta name="author" content="Bárbara Monedeiro">
      <link rel="icon" href="img/favicon.ico">
      <title>CRUD - Clientes - Teste Prático Consulta Já</title>
      <!-- Bootstrap core CSS -->
      <link href="css/bootstrap.min.css" rel="stylesheet">
      <link href="css/padrao.css" rel="stylesheet">
      <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
      <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
      <!-- Custom styles for this template -->
      <link href="css/jumbotron-narrow.css" rel="stylesheet">
    <script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
  </head>
  <body>
    <div class="container">
      <div class="header clearfix">
        <ul class="nav nav-pills">
          <li role="presentation" class="active"><a href="index.php">Home</a></li>
          <li role="presentation" class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
              Clientes <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
              <li role="presentation"><a href="clientes.php?acao=listar">Listar</a></li>
              <li role="presentation"><a href="clientes.php?acao=cadastrar">Cadastrar</a></li>
            </ul>
          </li>
          <li role="presentation" class="dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
              Grupos <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
              <li role="presentation"><a href="grupos.php?acao=listar">Listar</a></li>
              <li role="presentation"><a href="grupos.php?acao=cadastrar">Cadastrar</a></li>
            </ul>
          </li>
        </ul>
        <h2>Gerenciador de Clientes</h2>
      </div>


      